import './App.css';
import Demo4 from './chap4/Demo4';
import Demo5 from './chap4/Demo5';
import LoopDemo from './chap4/LoopDemo';
import FormDemo from './chap4/FormDemo';
import Student from './chap4/Student';
import InvestmentCalculator from './chap4/InvestmentCalculator';
import FetchDemo from './chap14/FetchDemo';
import CreatePost from './chap14/CreatePost';
import UpdatePost from './chap14/UpdatePost';
import ListPost from './chap14/ListPost';
import RandomNumber from './chap14/RandomNumber';
import GuessGame from './chap14/GuessGame';
import NotFound from './NotFound';
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Menu from './Menu';
import EditPost from './chap14/EditPost';

function App() {
  return (
    <Router>
      <Menu />
      <Routes>
        <Route path='/' element={<GuessGame />}/>
        <Route path='/list-post' element={<ListPost />}/>
        <Route path='/create-post' element={<CreatePost />}/>
        <Route path='/update-post' element={<UpdatePost />}/>
        {/* /edit-post/10 */}
        <Route path='/edit-post/:id' element={<EditPost />}/>
        {/* not match, run this component */}
        <Route path='*' element={<NotFound />}/>
      </Routes>
    </Router>
  );

  //return (
    // <Demo4 />
    // <Demo5 />
    // <LoopDemo />
    // <FormDemo />
    // <InvestmentCalculator />
    // <Student />
    // <FetchDemo />
    // <CreatePost />
    // <UpdatePost/>
    // <RandomNumber />
    // <ListPost />
    //<GuessGame />
  //);
}

export default App;
