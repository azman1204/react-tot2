import { useState } from "react";

function Demo5() {
    const [showMessage, setShowMessage] = useState(false);

    function doToggle() {
        setShowMessage(!showMessage);
    }

    return (
        <div>
            <button onClick={ doToggle }>
                Click me to { showMessage ? 'hide' : 'show' } the rest
            </button>

            <div>
                { showMessage && <p>i am the content that should be hidden by default</p> }
            </div>
        </div>
    );
}

export default Demo5;