import { useRef, useState } from 'react';

function InvestmentCalculator() {
    const amountRef = useRef();
    const yearsRef = useRef();
    const rateRef = useRef();
    const [result, setResult] = useState([]);

    function calculate() {
        let amount = parseInt(amountRef.current.value);
        let years  = yearsRef.current.value;
        let rate   = rateRef.current.value;
        let year = 2023;
        let arr = [];
        for(let i=0; i<years; i++) {
            let profit = Math.ceil(amount * rate / 100);
            let investment = {year, amount, profit};
            arr.push(investment);
            amount = amount + profit;
            year++;
        }
        setResult(arr);
        console.log(arr);
    }

    function showResult() {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <td>Year</td>
                        <td>Amount</td>
                        <td>Profit</td>
                    </tr>
                </thead>
                <tbody>
                    {result.map((investment, index) => (
                        <tr>
                            <td>{ investment.year }</td>
                            <td>{ investment.amount }</td>
                            <td>{ investment.profit }</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    }

    return (
        <div>
            Investment (RM) : <input type="text" ref={amountRef} />
            <br/>
            Duration (years) : <input type="text" ref={yearsRef} />
            <br/>
            Profit (%) : <input type="text" ref={rateRef} />
            <br/>
            <button onClick={calculate}>Generate</button>
            <hr/>
            { showResult() }
        </div>
    );
}
export default InvestmentCalculator;