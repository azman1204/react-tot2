import Demo5 from './Demo5';

function LoopDemo() {

    function demo() {
        let arr = [1,2,3,4,5];
        // this is normal js loop
        arr.forEach((num) => console.log(num));
    }

    const arr = [1,2,3,4,5];

    return (
        <div>
            { arr.map((num, index) => (
                <p key={index}>
                    Hello
                    <Demo5 />
                </p>
                )
            )}

            { demo() }
        </div>
    );
}

export default LoopDemo;