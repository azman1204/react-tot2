import { useRef, useEffect, useState } from 'react';
// useParam is used to read parameter from URL
import { useParams } from 'react-router-dom';

function EditPost() {
    const authorRef = useRef();
    const titleRef = useRef();
    const { id } = useParams();
    const [post, setPost] = useState({});

    useEffect(() => {
        let url    = 'http://localhost:3004/posts/' + id;
        let param  = {
            method : 'GET',
        };
        fetch(url, param)
        .then(res => res.json())
        .then(json => setPost(json));
    }, []);

    function doSave() {
        let author = authorRef.current.value;
        let title  = titleRef.current.value;
        let data   = {author, title};
        let url    = 'http://localhost:3004/posts';
        let param  = {
            method : 'POST',
            body: JSON.stringify(data),
            headers: {'Content-type' : 'application/json'}
        };
        fetch(url, param);
    }

    return (
        <div>
            <h2>Edit Post</h2>
            <div className="mb-3">
                <label className="form-label">Author</label>
                <input type="email" className="form-control" ref={authorRef} value={post.author} />
            </div>
            <div className="mb-3">
                <label className="form-label">Title</label>
                <textarea className="form-control"rows="3" ref={titleRef} value={post.title}></textarea>
            </div>
            <div className="mb-3">
                <button className='btn btn-primary' onClick={doSave}>Save</button>
            </div>
        </div>
    );
}

export default EditPost;