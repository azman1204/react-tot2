import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ListPost() {
    const [posts, setPosts] = useState([]);

    // listening to the changes of any state
    useEffect(() => {
        console.log('use effect is running...');
        allPosts();
    }, []);

    // retrieve all post from json placeholder website
    function allPosts() {
        fetch('http://localhost:3004/posts', {
            method: 'GET',
        }).then((res) => {
            return res.json();
        }).then((data) => {
            console.log(data);
            setPosts(data);
        });
    }

    function doDelete(id) {
        alert(id);
        fetch('http://localhost:3004/posts/'+id, {
            method: 'DELETE',
        }).then((res) => {
            return res.json(); // convert from string of JSON to JSON
        }).then((data) => {
            // operate over the data
            console.log(data);
            allPosts(); // refresh the data
        }).catch(err => console.log(err));
    }

    function doClick() {
        alert('you click me');
    }

    return (
        <>
            <div>
                {posts.map((data, index) => (
                    <div className='card mb-2'>
                        <div className='card-body' key={data.id}>
                            Title : { data.title }
                            <br/>
                            ID : { data.id }
                            <br/>
                            Author : { data.author } <br/>
                            {/* this method if we need to pass argument */}
                            <a href='##' className='btn btn-danger' onClick={() => doDelete(data.id)}>Delete</a>
                            {/* this work if no parameter passed */}
                            <button onClick={doClick}>Click Me</button>
                            <Link to={`/edit-post/${data.id}`} className='btn btn-success'>Edit</Link>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
}

export default ListPost;