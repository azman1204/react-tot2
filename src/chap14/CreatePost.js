import { useRef } from 'react';

function CreatePost() {
    const authorRef = useRef();
    const titleRef = useRef();

    function doSave() {
        let author = authorRef.current.value;
        let title  = titleRef.current.value;
        let data   = {author, title};
        let url    = 'http://localhost:3004/posts';
        let param  = {
            method : 'POST',
            body: JSON.stringify(data),
            headers: {'Content-type' : 'application/json'}
        };
        fetch(url, param);
    }

    return (
        <div>
            <div className="mb-3">
                <label className="form-label">Author</label>
                <input type="email" className="form-control" ref={authorRef} 
                placeholder="name@example.com" />
            </div>
            <div className="mb-3">
                <label className="form-label">Title</label>
                <textarea className="form-control"rows="3" ref={titleRef}></textarea>
            </div>
            <div className="mb-3">
                <button className='btn btn-primary' onClick={doSave}>Save</button>
            </div>
        </div>
    );
}

export default CreatePost;