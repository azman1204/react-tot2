import { useState, useEffect } from 'react';

function FetchDemo() {
    const [posts, setPosts] = useState([]);

    // listening to the changes of any state
    useEffect(() => {
        console.log('use effect is running...');
        allPosts();
    }, []);

    // retrieve all post from json placeholder website
    function allPosts() {
        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'GET',
        }).then((res) => {
            return res.json();
        }).then((data) => {
            console.log(data);
            setPosts(data);
        });
    }

    return (
        <>
            <ul>
                {posts.map((data, index) => (
                    <li key={data.id}>
                        Title : { data.title }
                        <br/>
                        ID : { data.id }
                        <br/>
                        Body : { data.body }
                    </li>
                ))}
            </ul>
        </>
    );
}

export default FetchDemo;