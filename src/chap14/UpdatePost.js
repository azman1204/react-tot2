import { useRef } from 'react';

function UpdatePost() {
    const authorRef = useRef();
    const titleRef  = useRef();
    const idRef     = useRef();

    function doSave() {
        let author = authorRef.current.value;
        let title  = titleRef.current.value;
        let id     = idRef.current.value;
        let data   = {author, title};
        let url    = 'http://localhost:3004/posts/'+id;
        let param  = {
            method : 'PATCH',
            body: JSON.stringify(data),
            headers: {'Content-type' : 'application/json'}
        };
        fetch(url, param);
    }

    return (
        <div>
            <div className="mb-3">
                <label className="form-label">ID</label>
                <input type="text" className="form-control" ref={idRef} />
            </div>
            <div className="mb-3">
                <label className="form-label">Author</label>
                <input type="text" className="form-control" ref={authorRef} />
            </div>
            <div className="mb-3">
                <label className="form-label">Title</label>
                <textarea className="form-control"rows="3" ref={titleRef}></textarea>
            </div>
            <div className="mb-3">
                <button className='btn btn-primary' onClick={doSave}>Save</button>
            </div>
        </div>
    );
}

export default UpdatePost;