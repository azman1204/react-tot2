import { useState, useEffect, useRef } from 'react';

function RandomNumber() {
    const [randNum, setRandNum] = useState(0);
    const nombor = useRef();
    // think this is like constructor
    useEffect(() => {
        doRandom();
    }, []);

    function doRandom() {
        let num = Math.floor(Math.random() * 100);
        setRandNum(num);
        alert(nombor.current.value);
        // alert(num);
    }

    function showMessage() {
        if (randNum < 50)
            return <p>Number less than 50</p>
        else
            return <p>Number greater than 50</p>
    }

    return (
        <div>
            <button onClick={doRandom}>Click Me</button>
            <input type="text" ref={nombor} />
            Rand Num : { randNum }
            <hr/>
            { showMessage() }
        </div>
    );
}

export default RandomNumber;