import { useEffect, useState, useRef } from "react";

function GuessGame() {
    const [randNo, setRandNo] = useState();
    const [msg, setMsg] = useState();
    const [trialNo, setTrialNo] = useState(0);
    const guessRef = useRef();

    useEffect(() => {
        setRandNo(genRandNo());
    }, []);

    function genRandNo() {
        let no = Math.floor(Math.random() * 100);
        return no;
    }

    function playAgain() {
        //alert('play again');
        setRandNo(genRandNo());
        setTrialNo(0);
        setMsg('');
    }

    function doCheck() {
        let trial = trialNo + 1;
        setTrialNo(trial);
        let guessNo = guessRef.current.value;
        let msg = "";
        if (guessNo > randNo) {
            msg = "Guess lower"
        } else if (guessNo < randNo) {
            msg = "Guess higher";
        } else {
            msg = <p>
                    You win after { trialNo } trial 
                    <button onClick={playAgain}>Play Again ?</button>
                  </p>;
        }
        setMsg(msg);
    }

    return (
        <div>
            { randNo }
            <h1>Guess Game</h1>
            Guess a number : <input type="text" ref={guessRef}/>
            <button onClick={doCheck}>Go</button>
            <hr/>
            { msg }
        </div>
    );
}

export default GuessGame;