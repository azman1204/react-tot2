function NotFound() {
    return (
        <div>
            <h6>Sorry, Page not found</h6>
        </div>
    );
}

export default NotFound;